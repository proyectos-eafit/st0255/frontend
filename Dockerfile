FROM node:12.18.0-alpine3.9

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --production

FROM node:12-slim

WORKDIR /app
COPY --from=0 /app .
COPY . .

RUN npm install
RUN npm run build:tailwind
RUN npm run build

EXPOSE 3000
CMD [ "node", "__sapper__/build" ]
